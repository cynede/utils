#!/usr/bin/env bash

sudo cp -rf .ssh /root/
sudo cp -rf .zsh /root/
sudo cp -f .zshrc /root/
sudo cp -f .gitconfig /root/.gitconfig
sudo chown root /root/.gitconfig

sudo rm -rf /root/.emacs.d
sudo cp -rf .emacs.d /root/
