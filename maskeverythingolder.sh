#!/usr/bin/env bash

for x in $(find -iname '*.ebuild'); do
    noe=${x/.ebuild/};
    nod=${noe/.\//<};
    echo ${nod/\/*\//\/};
done > /etc/portage/package.mask/overlay.mask
cat /etc/portage/package.mask/overlay.mask > /etc/entropy/packages/package.mask