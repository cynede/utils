#!/usr/bin/env bash

sudo rm -rf ~/.adobe*
sudo rm -rf ~/.cabal*
sudo rm -rf ~/.cache*
sudo rm -rf ~/.gnome2*
sudo rm -rf ~/.Skype*
sudo rm -rf ~/.mozilla
sudo rm -rf ~/.thumbnails
sudo rm -rf ~/.wine
sudo rm -rf ~/Desktop
sudo rm -rf ~/.bash_history
sudo rm -rf ~/.directory
sudo rm -rf ~/.com.*
sudo rm -rf ~/.recently-used
sudo rm -rf ~/.serverauth*
sudo rm -rf ~/.smex-items
sudo rm -rf ~/.zcompdump
sudo rm -rf ~/.zsh_history
sudo rm -rf ~/.veracity-logs

sudo emerge --depclean
sudo eclean-dist
