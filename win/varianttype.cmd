@echo off

cls
SET EnableNuGetPackageRestore=true

if not exist "tools\FAKE\tools\Fake.exe" (
	"tools\nuget\nuget.exe" "install" "FAKE" "-OutputDirectory" "tools" "-ExcludeVersion" "-Prerelease"
	)

start tools\FAKE\tools\Fsi.exe ..\varianttype.fsx