open System
open System.IO

let overlay = @"/home/gentoo-haskell"
let sync = true
let sudo = true

let shell cmd args path =
    let proc =
        match path with
            | "" -> new System.Diagnostics.ProcessStartInfo(cmd)
            | p -> new System.Diagnostics.ProcessStartInfo(cmd, p)
    proc.RedirectStandardOutput <- true
    proc.UseShellExecute <- false
    proc.Arguments <- args
    let p = System.Diagnostics.Process.Start(proc)
    let tool_output = p.StandardOutput.ReadToEnd()
    p.WaitForExit()
    tool_output
let shellx a b c = printf "%s" <| shell a b c
let shellxf a b c = printf "%s\n" <| shell a b c

let Resolve() =
    let troubles = File.ReadAllLines("missing")
    (*Missing metadata processing*)
    troubles
    |> Seq.filter(fun x -> x.Contains("metadata.xml"))
    |> fun missingMetadata ->
        if Seq.length missingMetadata > 0 then
            printf "detected files with missing metadata, resolving\n"
        missingMetadata
        |> Seq.iter(fun f ->
            let data = f.Trim().Split('/')
            let dir = data.[0]
            let package = data.[1]
            if not <| Directory.Exists(@"tmp/" + dir) then
                shellx "mkdir" (@"tmp/" + dir) ""
            if not <| Directory.Exists(@"tmp/" + dir + "/" + package) then
                printf "creating tmp dir for : %s" package
                shellx "mkdir" (@"tmp/" + dir + "/" + package) ""
            let meta1 = @"tmp/" + dir + @"/" + package + @"/metadata.xml"
            let meta2 = overlay + @"/" + dir +  @"/" + package + @"/metadata.xml"
            if not <| File.Exists(meta1) then
                shellx "hackport" ("--overlay-path=\"tmp\" merge " + package) ""
            if sudo then shellx "sudo" ("cp -f " + meta1 + " " + meta2) ""
            else shellx "cp" ("-f " + meta1 + " " + meta2) "")


if not <| Directory.Exists("tmp") then
    shellxf "mkdir" "tmp" ""
shellx "ls" "-a tmp" ""
if sync then shellxf "hackport" "--overlay-path=\"tmp\" update" ""
Resolve()
