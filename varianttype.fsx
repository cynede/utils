#light (*
	exec fsharpi --exec $0 $@
*)
/// --------------------------- CStruct To Variant // An light F# WinForm application -----------------------
open System;            open System.Text
open System.Drawing;    open System.Windows.Forms
/// ----------------------------------------------------------------------------------------------------------
let form = new Form()
form.Width              <- 800
form.Height             <- 750
form.FormBorderStyle    <- FormBorderStyle.Fixed3D
form.Text               <- "CStruct To Variant"
form.Font               <- new Font("Lucida Console",12.0f,FontStyle.Regular,GraphicsUnit.Point)
/// ----------------------------------------------------------------------------------------------------------
let l1 = new Label();;          let l2 = new Label();;
let r1 = new RichTextBox();;    let r2 = new RichTextBox();;
let b1 = new Button();;         let b2 = new Button();; let b3 = new Button();;
l1.Location <- Point(10,2);     l1.Text <- "STLStruct";
l2.Location <- Point(10,320);   l2.Text <- "Variant";
r1.Location <- Point(10,30);    r1.Size <- Size(770,270);
r2.Location <- Point(10,350);   r2.Size <- Size(770,300);
b1.Location <- Point(40,650);   b1.Size <- Size(150,50);    b1.Text <- "Exit";
b2.Location <- Point(580,650);  b2.Size <- Size(150,50);    b2.Text <- "Apply";
b3.Location <- Point(360,650);  b3.Size <- Size(150,50);    b3.Text <- "GVS";
/// ----------------------------------------------------------------------------------------------------------
let processX is_gvS =
    let counter = ref -1
    let ls ns m = if (ns : string).Length <= m then String.Join("",[for i in [ns.Length..m] -> " "]) else ""
    r2.Text <- String.Join(Environment.NewLine,
                [for line in r1.Lines do
                    let nsplitted = [for s in line.Split(' ','\r','\n','\t',';') do
                                        if s <> "" && not <| String.IsNullOrEmpty(s) then
                                            yield s]
                    if nsplitted.Length > 1 then
                        let (t,name) = 
                            nsplitted.[0].ToLower() |> fun firsttype ->
                                if firsttype = "unsigned" then
                                        ("u" + nsplitted.[1].ToLower(), nsplitted.[2])
                                else    (firsttype,                     nsplitted.[1])

                        let tvar =  match t with
                                    | "float"                       -> "VT_R4"
                                    | "double"                      -> "VT_R8"
                                    | "uint"    | "dword"           -> "VT_UI4"
                                    | "int"     | "__int32"         -> "VT_I4"
                                    | "short"   | "char"            -> "VT_I2"
                                    | "ushort"  | "uchar" | "word"  -> "VT_UI2"
                                    | "bool"                        -> "VT_BOOL"
                                    | "time_t"  | "__time32_t"      -> "VT_UI4"
                                    | _                             -> "VT_R4"
                                    
                        let gvS = match tvar with
                                    | "VT_R4"   -> "fltVal"
                                    | "VT_UI2"  -> "uiVal"
                                    | "VT_I2"   -> "iVal"
                                    | "VT_UI4"  -> "uintVal"
                                    | "VT_I4"   -> "intVal"
                                    | _         -> "uintVal"

                        let node n =
                            counter := !counter + 1
                            if is_gvS then
                                "} else if (pot->numPar < " + (!counter + 1).ToString() + ") {"
                                    + Environment.NewLine
                                    + "    pot->Value." + gvS + " = pCurPar->stat." + n + ";"
                            else "\tL" + "\"" + n + "\"," + ls n 20
                                    + tvar + "," + ls tvar 10 
                                    + "false, //" + (!counter).ToString()

                        if name.Contains("[") && name.Contains("]") then
                            let namesplit = name.Split('[',']')
                            if namesplit.Length > 1 then
                                let countn = Int32.Parse namesplit.[1]
                                if is_gvS then
                                    let zx = !counter + 1
                                    for i in [1..(countn - 1)] do counter := !counter + 1
                                    yield node (namesplit.[0] + "[pot->numPar - " + zx.ToString() + "]")
                                else for i in [1..countn] do
                                        yield node (namesplit.[0] + i.ToString())
                        else            yield node name])

b1.Click.Add(fun _ -> ignore <| form.Close())
b2.Click.Add(fun _ -> processX false)
b3.Click.Add(fun _ -> processX true)
/// ----------------------------------------------------------------------------------------------------------
form.Controls.AddRange [|l1; l2; b1; b2; b3; r1; r2|]; Application.Run(form);
/// ----------------------------------------------------------------------------------------------------------
