#light (*
	exec fsharpi --exec $0 $@
*)
open System

let private main (args: string []) =
    let gen() = printfn "%s" <| Guid.NewGuid().ToString().ToUpper()
    if args.Length > 0 then
        let s,count = Int32.TryParse(args.[0])
        if s then for i in [1..count] do gen()
             else gen()
    else gen()
    0

#if INTERACTIVE
fsi.CommandLineArgs |> Array.toList |> List.tail |> List.toArray |> main
#else
[<EntryPoint; STAThread>]
let entryPoint args = main args
#endif
